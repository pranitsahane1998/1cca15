public class Question2 {
    public static void main(String[] args) {

                int[]arr1 = {10,20,30};
                int[]arr2 = {40,50,60};

                int[]result = new int[arr1.length+arr2.length];
                int count = 0;

                for(int i = 0; i<arr1.length; i++) {
                    result[i] = arr1[i];
                    count++;
                }

                for(int j = 0;j<arr2.length;j++) {
                    result[count++] = arr2[j];
                }

                for(int i = 0;i<result.length;i++) {
                    System.out.print(result[i] + " ");
                };
            }
        }

