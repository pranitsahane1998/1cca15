import java.util.Scanner;

public class Question3 {
    public static void main(String[] args) {
        Scanner s1 = new Scanner(System.in);
        System.out.println("Enter a Word : ");
        String str = s1.next();
        System.out.println("OLD STRING IS :" + str);
        String str2 = swapFirstndLast(str);
        System.out.println("NEW STRING IS: " + str2);
    }

    public static String swapFirstndLast(String str1)
    {
        if (str1.length() < 2)
        {
            return str1;
        }
        else
        {
            char ch[] = str1.toCharArray();

            char temp = ch[0];
            ch[0] = ch[ch.length - 1];
            ch[ch.length - 1] = temp;

            String sc1 = "";
            for (int i = 0; i < ch.length; i++)
            {

                sc1 = sc1 + ch[i];
            }
            return sc1;
        }


/////////////////////////////////////////////////////////////////////////////
//        if(str1.length()<2){
//            return str1;
//        }
//        else{
//            return str1.charAt(str1.length()-1)+str1.substring(1,str1.length()-1)+str1.charAt(0);
//         }
    }
}

