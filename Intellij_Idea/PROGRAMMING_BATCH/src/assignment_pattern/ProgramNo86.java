package assignment_pattern;

public class ProgramNo86 {

    public static void main(String[] args) {

        int line=5;
        int star=5;

        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star; j++) {
                if(i==j)
                {
                    System.out.print("O");
                }
                else
                {
                    System.out.print("X");
                }
            }
            System.out.println();
        }
    }
}
