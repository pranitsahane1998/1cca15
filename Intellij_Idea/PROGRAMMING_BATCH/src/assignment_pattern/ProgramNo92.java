package assignment_pattern;

public class ProgramNo92 {
    public static void main(String[] args) {

        int line=5;
        int star=5;
        char ch='N';

        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star; j++) {
                if(i==j)
                {
                    System.out.print(ch);
                }
                else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
