package assignment_pattern;

public class ProgramNo80 {
    public static void main(String[] args) {

        int line =5;
        int star=1;
        int space=4;
        int space1=4;
        int star1=1;

        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star; j++) {
                System.out.print("*");
            }

            for (int s = 0; s < space; s++) {
                System.out.print(" ");
            }

            for (int j = 0; j < space1; j++) {
                System.out.print(" ");
            }

            for (int j = 0; j < star1; j++) {
                System.out.print("*");
            }
            System.out.println();
            star++;
            space--;
            space1--;
            star1++;
        }

    }
}
