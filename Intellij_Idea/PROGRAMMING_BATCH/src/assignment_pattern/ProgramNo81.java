package assignment_pattern;

public class ProgramNo81 {
    public static void main(String[] args) {

        int line=5;
        int star=1;
        int no1=1;
        int space=4;
        int space1=4;
        int star1=1;
        int no4=1;
        for (int i = 0; i < line; i++) {
            int no2=no1;
            int no3=no4;
            for (int j = 0; j < star; j++) {
                System.out.print(no2++);
            }
            for (int j = 0; j< space; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < space1; j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star1; j++) {
                System.out.print(no3);
                no3--;
            }
            System.out.println();
            star++;
            space--;
            space1--;
            star1++;
            no4++;
        }
    }
}
