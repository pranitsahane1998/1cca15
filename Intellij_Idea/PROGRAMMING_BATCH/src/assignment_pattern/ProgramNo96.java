package assignment_pattern;

public class ProgramNo96 {
    public static void main(String[] args) {

        int line=5;
        int star=1;
        int space=line-1;

        for (int i = 0; i < line; i++) {
            for (int s = 0; s < space; s++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                if(i==4)
                {
                    System.out.print("*");

                }
                else if(j==0||i==j)
                {
                    star=9;
                    System.out.print("* ");
                }
                else {
                    System.out.print("   ");
                }
            }
            System.out.println();
            star+=2;
            space--;
        }

    }
}
