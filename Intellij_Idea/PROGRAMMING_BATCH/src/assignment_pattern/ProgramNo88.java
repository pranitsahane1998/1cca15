package assignment_pattern;

public class ProgramNo88 {
    public static void main(String[] args) {

        int line =5;
        int star=5;
        char ch1='A';
        char ch2='B';
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star; j++) {
                if(i==j||i+j==4)
                {
                    System.out.print(ch1);
                }
                else
                {
                    System.out.print(ch2);
                }
            }
            System.out.println();
        }
    }
}
