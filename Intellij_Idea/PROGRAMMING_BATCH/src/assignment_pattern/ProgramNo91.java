package assignment_pattern;

public class ProgramNo91 {
    public static void main(String[] args) {

        int star=1;
        int line=5;
        int space=line-1;
        char ch='N';

        for (int i = 0; i < line; i++) {
            for (int s = 0; s < space; s++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                System.out.print(ch);
            }

            System.out.println();
            space--;

        }
    }
}
