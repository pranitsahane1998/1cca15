package assignment_pattern;

public class ProgramNo95 {
    public static void main(String[] args) {

        int star=5;
        int line=5;

        for (int i = 0; i < line; i++) {
            for (int j = 0; j < star; j++) {
                if(i==0||i==j||j==4)
                {
                    System.out.print("*");
                }
                else
                {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
