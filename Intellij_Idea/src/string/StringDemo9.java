package string;

import java.util.Scanner;

public class StringDemo9 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);

        System.out.println("ENTER NAME");
        String n=sc.nextLine();
        String name1=n.trim();
        String name=name1.toUpperCase();

        if(!name.isEmpty())
        {
            System.out.println("VALID NAME");
        }
        else
        {
            System.out.println("invalid name");
            System.exit(0);
        }

        System.out.println("ENTER EMAIL");
        String email=sc.nextLine();

        if(email.contains("@") && email.contains("."))
        {
            System.out.println("VALID GMAIL");
        }
        else
        {
            System.out.println("INVALID EMAIL");
            System.exit(0);
        }


        System.out.println("ENTER PASSWORD");
        String pass=sc.next();

        if(pass.length()>=8 && !(pass.contains(name)))
        {
            System.out.println("CONFIRM PASSWORD");
            String confirm = sc.next();

            if (pass.equals(confirm)) {
                System.out.println("WELCOME :" + name);
            }

            else
            {
                System.exit(0);
            }

        }
        }
    }

