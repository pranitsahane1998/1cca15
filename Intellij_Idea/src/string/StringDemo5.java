package string;

import java.util.Locale;
import java.util.Scanner;

public class StringDemo5 {
    public static void main(String[] args) {

        Scanner sc=new Scanner(System.in);
        System.out.println("ENTER STRING");
        String s=sc.nextLine();
        String s2=s.toLowerCase();
        char[]data=s2.toCharArray();

        int vCount=0;
        int cCount=0;

        for (int a=0;a<data.length;a++)
        {
            if(data[a]=='a'||data[a]=='e'||data[a]=='i'||data[a]=='o'||data[a]=='u')
            {
                vCount++;
            }
            else
            {
                cCount++;
            }
        }
        System.out.println("TOTAL NO OF VOWELS :"+vCount);
        System.out.println("TOTAL NO OF CONSONANT :"+cCount);
    }
}
