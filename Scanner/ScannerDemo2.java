import java.util.Scanner;
class ScannerDemo2
{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("ENTR FIRST NUMBER:");
		double n1=sc.nextDouble();
		System.out.println("ENTER SECOND NUMBER:");
		double n2=sc.nextDouble();

		addition(n1,n2);
		substraction(n1,n2);
		multiplication(n1,n2);
		division(n1,n2);
}
	static void addition(double n1,double n2)
	{
		double result=n1+n2;
		System.out.println("addition:"+result);
	}

	static void substraction(double n1,double n2)
	{
		double result=n1-n2;
		System.out.println("substraction:"+result);
	}

	static void multiplication(double n1,double n2)
	{
		double result=n1*n2;
		System.out.println("multiplication:"+result);
	}

	static void division(double n1,double n2)
	{
		double result=n1/n2;
		System.out.println("division:"+result);
	}

}