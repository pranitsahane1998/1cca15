import java.util.Scanner;
class ScannerDemo3
{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("ENTER TOTAL NO OF MATCHES:");
		double total=sc.nextDouble();

		System.out.println("ENTER TOTAL NO OF WINS:");
		double win=sc.nextDouble();

		System.out.println("ENTER TOTAL NO OF LOSS:");
		double loss=sc.nextDouble();

		System.out.println("ENTER TOTAL NO OF DRAW:");
		double draw=sc.nextDouble();

		CalculatePoint(total,win,loss,draw);
	}

	static void CalculatePoint(double t,double w,double l,double d)
	{
		double point=(t*5)+(l*-2)+(d*3);
		double winPercentage=t/w;

		System.out.println("total point:"+point);
		System.out.println("winPercentage:"+winPercentage);
	}
}