class OperatorDemo
{
	public static void main(String[]args)
	{
		System.out.println(10+5); 			//15
		System.out.println(12.25+36.78);	//49.03
		System.out.println(10+35.25);		//45.25

		System.out.println(10+"java");		//10JAVA
		System.out.println(10+5+"java");	//15JAVA
		System.out.println("java"+10+20);	//JAVA1020
		System.out.println("java"+(10+20)); //JAVA30

		System.out.println('A'+'B');		//131
		System.out.println('A'+10);			//75
		System.out.println('C'+"java");		//CJAVA
		System.out.println("core"+"java");	//COREJAVA
		System.out.println("10"+true);		//10TRUE

		//System.out.println(10+true); 		//ERROR
		
	}
}
