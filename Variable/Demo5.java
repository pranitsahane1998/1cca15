class Demo5
{
	public static void main(String[] args) {
		
		int qty=20;
		double price=1000;

		double total=qty*price;

		double discount=total*0.1;

		double finaldiscount=total-discount;

		System.out.println("TOTAL WITHOUT DISCOUNT:"+total);
		System.out.println("TOTAL WITH DISCOUNT:"+finaldiscount);
		
	}
}