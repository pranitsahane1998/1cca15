class AddCon
{
	public static void main(String[] args) 
	{
		System.out.println(10+'A'+"SQL"+5+2);					//75SQL52
		System.out.println('A'+"JAVA"+5+4+'B');					//AJAVA54B
		System.out.println("PUNE"+10+"MUMBAI"+20+"DELHI"+30);	//PUNE10MUMBAI20DELHI30
		//System.out.println(true+10+"SQL"+5+10);				//ERROR
		System.out.println("JAVA"+10+'A'+true+"SQL");			//JAVA10ATRUESQL
		System.out.println(false+"5"+'A'+"J2EE"+'B');			//FALSE5AJ2EEB
		System.out.println('C'+"J"+10+"SQL"+(10+5+'A')+'D');	//CJ10SQL80D
	}
}