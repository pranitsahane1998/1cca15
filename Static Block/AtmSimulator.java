import java.util.Scanner;

class AtmSimulator
{

	static Scanner s=new Scanner(System.in);
	static double accountBalance=5000.00;
	static int pin;

	static
	{
		System.out.println("SELECT LANGUAGE");
		System.out.println("1:ENGLISH\n2.MARATHI\n3.HINDI");

		int choice=s.nextInt();

		if(choice==1)
		{
			System.out.println("SELECTED LANGUAGE ENGLISH");
		}
		else if(choice==2)
		{
			System.out.println("SELECTED LANGUAGE MARATHI");
		}
		else if(choice==3)
		{
			System.out.println("SELECTED LANGUAGE HINDI");
		}
		else{

			System.exit(0);
		}
	}

	static
	{
		System.out.println("ENTER PIN");
		pin=s.nextInt();
	}

	public static void main(String[] args) {
		
		if(pin==1000)
		{
			System.out.println("1.WITHDRAW\n2.CHECK BALANCE\n3.CHANGE PIN");
			int choice=s.nextInt();

			if(choice==1)
			{
				System.out.println("ENTER AMMOUNT");
				double amt=s.nextDouble();
				withdraw(amt);
			}
			else if(choice==2)
			{
				checkBalance();
			}

			else if(choice==3)
			{
				System.out.println("ENTER NEW PIN");
				int newPin=s.nextInt();
				changePin(newPin);
			}

			else 
			{
				System.out.println("INVALID CHOICE");
			}
		}

		else 
		{
			System.out.println("INVALID PIN");
		}
	}

	static void withdraw(double amt)
	{
		if(amt<=accountBalance)
		{
			accountBalance-=amt;
			System.out.println(amt+"RS debited From Account");
			System.out.println("REMANING ACCOUNT BALANCE:=>"+accountBalance);
		}
		else 
		{
			System.out.println("INSUFFICIENT BALANCE");

		}
	}

	static void checkBalance()
	{
		System.out.println("ACCOUNT BALANCE:=>"+accountBalance);
	}

	static void changePin(int newPin)
	{
		pin=newPin;
	}
}