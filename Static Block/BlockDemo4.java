class GoogleAccount
{
	static int count=0;

	{
		System.out.println("ACCOUNT CREATED");
		createProfile();
		count++;
	}

	void createProfile()
	{
		System.out.println("PROFILE CREATED");
	}
}

class BlockDemo4
{
	public static void main(String[] args) {
		
		GoogleAccount g1=new GoogleAccount();
		GoogleAccount g2=new GoogleAccount();
		GoogleAccount g3=new GoogleAccount();

		System.out.println("TOTAL NO OF ACCOUNT CREATED:=> "+GoogleAccount.count);
	}
}