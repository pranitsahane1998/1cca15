class BlockDemo1
{
	public static void main(String[] args) {
		
		System.out.println("MAIN METHOD");
	}

	static
	{

		System.out.println("FISRT STATIC BLOCK");
	}

	static
	{

		System.out.println("SECOND STATIC BLOCK");
	}
}

// output:=	"FISRT STATIC BLOCK"
// 			"SECOND STATIC BLOCK"
// 			"MAIN METHOD"