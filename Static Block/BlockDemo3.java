class BlockDemo3
{
	static
	{
		System.out.println("STATIC BLOCK");
	}

	public static void main(String[] args) {
		
		System.out.println("MAIN METHOD");
		BlockDemo3 b1=new BlockDemo3();
		BlockDemo3 b2=new BlockDemo3();
	}

	//NON-STATIC BLOCK
	{
		System.out.println("NON-STATIC METHOD");
	}
}