class StringDemo1
{
	public static void main(String[] args) {
		
		//CONSTANT
		String s1="PUNE";
		String s2="MUMBAI";
		String s3="PUNE";

		//NON-CONSTANT
		String str1=new String("Mumbai");
		String str2=new String("PUNE");
		String str3=new String("PUNE");

		System.out.println(s1==s2);//FALSE
		System.out.println(s1==s3);//TRUE
		System.out.println(s1==str1);//FALSE
		System.out.println(s1.equals(str2));//TRUE
		System.out.println(s2.equals(str1));//FALSE
		System.out.println(s2==str3);//FALSE
		System.out.println(s2.equalsIgnoreCase(str1));//TRUE

	}
}