class StringDemo3
{
	public static void main(String[] args) {
		
		String str="SOFTWARE DEVELOPER";

		// System.out.println(str.length());	//18
		// System.out.println(str.charAt(12));	//E
		// System.out.println(str.indexOf('W'));//4
		// System.out.println(str.lastIndexOf('E'));//16
		// System.out.println(str.contains("EVE"));//TRUE
		// System.out.println(str.startsWith("SOFT"));//TRUE
		// System.out.println(str.endsWith("PER"));// TRUE
		// System.out.println(str.substring(9));//DEVELOPER
		// System.out.println(str.substring(0,8));//SOFTWARE
		// System.out.println(str.toUpperCase());//SOFTWARE DEVELOPER
		// System.out.println(str.toLowerCase());//software developer
		System.out.println(str.length());

		// for(int i=0;i<str.length();i++)
		// {
		// 		System.out.println(i+"=>"+str.charAt(i));
		// 	}
		}
	}
