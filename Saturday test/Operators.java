class Operators
{
	public static void main(String[] args) {

	int i, mask=01;
	int c=48;

	for(i=1;i<=5;i++)
	{
		System.out.println(c|mask);
		mask=mask<<1;
	}

	System.out.println(mask);

///////////(2)//////////////////////////////////////////////////////

		int x=9;
		int y=12;
		int z=3;

		int exp1=x-y/z+z*2-1;

		int exp2=(x-y)/z+((z*2)-1);

		System.out.println(exp1);	//10
		System.out.println(exp2);	//4

//////////////////(3)//////////////////////////////////////////////////

		int x=9;
		int y=12;
		int a=2;
		int b=4;
		int c=6;

		int exp1=(3+4*9)/5-10*(12-5)*(2+4+6)/9+9*(4/9+(9+9)/12);
		System.out.println(exp1);	//-77

///////////////////(5)//////////////////////////////////////////////////

		int x=1;
		int y=2;
		int z=5;

		System.out.println(!((1+2)==(1+2)));			//false
		System.out.println(!(1==5));					//true
		System.out.println(!(5>1));					//false
		if(!(1==2) && ((2+5)>2) && (!((5-3)==0)))	//true
		{
			System.out.println("H!!!");
		}

		else{
			System.out.println("bye");
		}


///////////////////(6)//////////////////////////////////////////////////////

		int x=20;
		int y=30;
		int z=50;

		y-=x+z;
		z*=x*y;

		System.out.println(y);	//-40
		System.out.println(z);	//-40000

/////////////////(8)/////////////////////////////////////////////////////

		double x=10.5;
		x/=4+2.5*2.5;

		System.out.println(x);	//1.024

////////////////////(9)////////////////////////////////////////////////

		int a,b,c;
		a=b=c=5;
		int exp=a+b++ + ++c;

		//System.out.println(a+"\n"+b+"\n"+c);
		System.out.println(exp);

/////////////////////(10)////////////////////////////////////////////

		int a,b,exp=10;
		a=b=5;
		exp=exp+ ++a * b++;
		System.out.println(exp);	//40

////////////////////(11)//////////////////////////////////////////////

		int a,b,exp=10;
		a=b=5;

		exp=exp* ++a * 10/b++ + --a;

		System.out.println(exp);	//125
		
	}
}